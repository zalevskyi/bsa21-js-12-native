export interface ContactData {
  id: string;
  name: string;
  phone: string;
}
