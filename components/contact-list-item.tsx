import * as React from 'react';

import { Text, View } from 'react-native';

export function ContactListItem(
  props: { name: string; phone: string } & View['props'],
) {
  return (
    <View style={props.style}>
      <Text>{props.name}</Text>
      <Text>{props.phone}</Text>
    </View>
  );
}
