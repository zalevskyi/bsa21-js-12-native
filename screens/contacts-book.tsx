import React from 'react';
import {
  FlatList,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import { ContactListItem } from '../components/contact-list-item';
import { useContacts } from '../hooks/useContacts';

export function ContactsBook({ navigation }: any) {
  const { contacts, search, setSearch } = useContacts();

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.search}
        onChange={evt => setSearch(evt.nativeEvent.text)}
        defaultValue={search}
      />
      <FlatList
        style={styles.constactList}
        data={contacts}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={evt =>
              navigation.navigate('Details', {
                name: item.name,
                phone: item.phone,
              })
            }>
            <ContactListItem
              name={item.name}
              phone={item.phone}
              style={styles.contact}
            />
          </TouchableOpacity>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0FFFF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
  },
  search: {
    backgroundColor: '#ADD8E6',
    width: '100%',
    padding: 10,
  },
  constactList: {
    width: '100%',
    padding: 10,
  },
  contact: {
    backgroundColor: '#87CEFA',
    padding: 10,
    width: '100%',
    marginTop: 10,
    borderRadius: 10,
  },
});
