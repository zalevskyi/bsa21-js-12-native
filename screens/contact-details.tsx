import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
} from 'react-native';
export function ContactDetails({ navigation, route }: any) {
  return (
    <View style={styles.container}>
      <Text>{route.params.name}</Text>
      <Text>{route.params.phone}</Text>
      <TouchableOpacity>
        <Text
          style={styles.call}
          onPress={() => {
            Linking.openURL(`tel:${route.params.phone}`);
          }}>
          Call
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F0FFFF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
  },
  call: {
    backgroundColor: '#87CEFA',
    padding: 10,
    width: '25%',
    marginTop: 10,
    borderRadius: 10,
  },
});
