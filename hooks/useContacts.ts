import { useEffect, useState } from 'react';
import * as Contacts from 'expo-contacts';
import { ContactData } from '../types/contacts';

export function useContacts() {
  const [contacts, setContacts] = useState<ContactData[]>([]);
  const [search, setSearch] = useState('');
  useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync({
          fields: [
            Contacts.Fields.ID,
            Contacts.Fields.Name,
            Contacts.Fields.PhoneNumbers,
          ],
          name: search,
        });
        setContacts(
          data
            .map(contact => {
              if (contact.name && contact.phoneNumbers) {
                if (contact.phoneNumbers[0].number) {
                  return {
                    id: contact.id,
                    name: contact.name,
                    phone: contact.phoneNumbers[0].number,
                  };
                }
              }
            })
            .filter(contact => contact !== undefined) as ContactData[],
        );
      }
    })();
  }, [search]);
  return { contacts, search, setSearch };
}
