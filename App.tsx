import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ContactsBook } from './screens/contacts-book';
import { createStackNavigator } from '@react-navigation/stack';
import { ContactDetails } from './screens/contact-details';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Contacts' component={ContactsBook} />
        <Stack.Screen name='Details' component={ContactDetails} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
